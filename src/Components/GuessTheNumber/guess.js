import React, { useState, useEffect } from "react";
import "./guess.css";
import "../../App.css";

const Guess = () => {
  const [message, setMessage] = useState("");
  const [disabled, setDisabled] = useState(false);
  const [clicks, setClicks] = useState(0);
  const [title, setTitle] = useState("🎲 Let's play Guess The Number! 🤔");
  const [generatedNum, setGeneratedNum] = useState("");

  useEffect(() => {
    clicks > 0 &&
      setTitle(`You have ${clicks} tries to guess the number between 1-10`);
  }, [clicks]);

  const start = () => {
    setDisabled(true);
    setGeneratedNum(Math.floor(Math.random() * 10) + 1);
    setClicks(3);
    setMessage("Give it a go!");
  };

  const guess = (e) => {
    e.preventDefault();
    console.log(generatedNum, clicks);

    if (clicks > 1) {
      if (e.target.input.value > generatedNum) {
        setMessage("Number is too big");
        setClicks(clicks - 1);
      } else if (e.target.input.value < generatedNum) {
        setMessage("Number is too small");
        setClicks(clicks - 1);
      } else {
        setClicks(clicks - 1);
        setMessage("You have guessed the number! 🎉");
        setDisabled(false);
      }
    } else {
      if (e.target.input.value > generatedNum) {
        setClicks(clicks - 1);
        setMessage("Number is too big and you ran out of attempts...");
        setDisabled(false);
      } else if (e.target.input.value < generatedNum) {
        setClicks(clicks - 1);
        setMessage("Number is too small and you run out of attempts");
        setDisabled(false);
      } else {
        setClicks(clicks - 1);
        setMessage("You have guessed the number! 🎉");
        setDisabled(false);
      }
    }
  };

  // use while
  // dynamic range and attempts
  // maybe difficulty levels

  return (
    <div className="guess">
      <h3 className="title">{title}</h3>
      <div className="flex">
        <button onClick={() => start()} disabled={disabled}>
          Start
        </button>
        <form onSubmit={(e) => (clicks > 0 ? guess(e) : null)}>
          <input
            type="number"
            className="input"
            disabled={!disabled}
            id="input"
          />
          <button style={{ display: "none" }}></button>
        </form>
      </div>
      <p>{message}</p>
    </div>
  );
};

export default Guess;
