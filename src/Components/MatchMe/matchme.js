import React, { useState } from "react";
import "./matchme.css";
import "../../App.css";

const Match = () => {
  const [person1, setPerson1] = useState("");
  const [person2, setPerson2] = useState("");
  const [answer, setAnswer] = useState("");
  const [disabled, setDisabled] = useState(false);

  const getName = () => {
    setDisabled(true);
    const tempPerson1 = window.prompt(`Please, write person1 name`, ``);
    setPerson1(tempPerson1);
    if (person1 == null) {
      setAnswer(`You stopped the test`);
      return `byebye`;
    }
    const tempPerson2 = window.prompt(`Please, write person2 name`, ``);
    setPerson2(tempPerson2);
    if (person2 == null) {
      setAnswer(`You stopped the test`);
      return `byebye`;
    }

    return matchThemUp(tempPerson1, tempPerson2);
  };

  var questions = [
    `🐶 Do you like pets 🐈`,
    `🍺 Do you like beer 🍻`,
    `🤓 Do you like books 📖`,
    `🚴‍♀️ Do you enjoy riding a bike 🚲`,
    `🎧 Do you like mainstream music the most 🎹`,
  ];

  var matchThemUp = (tempPerson1, tempPerson2) => {
    var person1Arr = [];
    var person2Arr = [];

    for (let i = 0; i < questions.length; i++) {
      let meh = window.prompt(`${questions[i]}, ${tempPerson1}? Y / N`, ``);
      if (meh === null) {
        setAnswer(`You stopped the test`);
        return `byebye`;
      }
      person1Arr.push(meh);
    }

    window.alert(`${tempPerson2} get ready!`);

    for (let i = 0; i < questions.length; i++) {
      let meh = window.prompt(`${questions[i]}, ${tempPerson2}? Y / N`, ``);
      if (meh === null) {
        setAnswer(`You stopped the test`);
        return `byebye`;
      }
      person2Arr.push(meh);
    }

    var percentage = 0;
    person1Arr.forEach(
      (answer, idx) =>
        answer === person2Arr[idx] && (percentage += 100 / person1Arr.length)
    );

    if (percentage > 50) {
      setAnswer(`👩‍❤️‍👩 ${tempPerson1} & ${tempPerson2}: you are a match! 👩‍❤️‍👨`);
    } else {
      setAnswer(`No match found. Keep looking. Plenty of fish in the sea`);
    }
  };

  return (
    <div className={`${!disabled && "blink"}`}>
      <h3 className="title">💝 Welcome to "Match Me"! 💌</h3>
      <button onClick={() => getName()} disabled={disabled} className="button">
        Start match test!
      </button>
      <h3>{answer}</h3>
    </div>
  );
};

export default Match;
