import React, { useState, useEffect } from "react";
import "./rockpaperscissors.css";
import "../../App.css";

const RPS = () => {
  const [decision, setDecision] = useState("");
  const [final, setFinal] = useState("Press Start");
  const [whatDidIChoose, setWhatDidIChoose] = useState("");
  const [whatDidCompChoose, setWhatDidCompChoose] = useState("");
  const [userLives, setUserLives] = useState(3);
  const [compLives, setCompLives] = useState(3);
  const choices = ["rock", "paper", "scissors"];
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    if (userLives === 0) {
      setFinal("YOU LOST THE GAME");
      setDecision("");
      setWhatDidIChoose("");
      setWhatDidCompChoose("");
      setDisabled(false);
    } else if (compLives === 0) {
      setFinal("YOU WIN THE GAME");
      setDecision("");
      setWhatDidIChoose("");
      setWhatDidCompChoose("");
      setDisabled(false);
    }
  }, [userLives, compLives]);

  const startGame = () => {
    setCompLives(3);
    setUserLives(3);
    setDisabled(true);
    setFinal(`Welcome! Choose a weapon 🧨`);
  };

  const handleClick = (userChoice) => {
    if (final !== "") {
      setFinal("");
    }
    if (userLives === 0 || compLives === 0) {
      setFinal("Please press Start to play again 🙂");
    } else {
      setWhatDidIChoose(`You chose ${userChoice}`);
      return getComputerChoice(userChoice);
    }
  };

  const getComputerChoice = (userChoice) => {
    const randomNumber = Math.floor(Math.random() * 3);
    let compChoice = choices[randomNumber];
    setWhatDidCompChoose(`Computer chose ${compChoice}`);
    return determineWinner(userChoice, compChoice);
  };

  const determineWinner = (userChoice, compChoice) => {
    setDecision("It's a tie");
    if (userChoice === "rock") {
      if (compChoice === "paper") {
        setDecision("Computer wins");
        setUserLives(userLives - 1);
      } else if (compChoice === "scissors") {
        setDecision("You win");
        setCompLives(compLives - 1);
      }
    } else if (userChoice === "paper") {
      if (compChoice === "rock") {
        setDecision("You win");
        setCompLives(compLives - 1);
      } else if (compChoice === "scissors") {
        setDecision("Computer wins");
        setUserLives(userLives - 1);
      }
    } else if (userChoice === "scissors") {
      if (compChoice === "rock") {
        setDecision("Computer wins");
        setUserLives(userLives - 1);
      } else if (compChoice === "paper") {
        setDecision("You win");
        setCompLives(compLives - 1);
      }
    }
  };

  return (
    <div className="rps">
      <h3 className="title">Let's play: 🤘 📄 ✂️</h3>
      <div className="imgCont">
        <div
          className={`pointer imgDiv ${!disabled && "nope"}`}
          onClick={() => disabled && handleClick("rock")}
        >
          <img
            src="https://pngimg.com/uploads/stone/stone_PNG13622.png"
            alt="rock"
            className="pointer img"
          />
        </div>
        <div
          className={`pointer imgDiv ${!disabled && "nope"}`}
          onClick={() => disabled && handleClick("paper")}
        >
          <img
            src="https://i.pinimg.com/originals/0d/b7/62/0db76210a9e1873e3e9f2bd6df438aa7.png"
            alt="paper"
            className="pointer img"
          />
        </div>
        <div
          className={`pointer imgDiv ${!disabled && "nope"}`}
          onClick={() => disabled && handleClick("scissors")}
        >
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/4/4c/Vectorel-scissors-2245884.png"
            alt="scissors"
            className="pointer img"
          />
        </div>
      </div>
      <div className="livesCont">
        <div className="lives">
          <p className="strong">My lives: </p>
          <p>{userLives}</p>
        </div>
        <div className="lives">
          <p className="strong">Computer lives: </p>
          <p>{compLives}</p>
        </div>
      </div>
      <div>
        <button
          onClick={() => startGame()}
          className={`${disabled ? "nope2" : "startBtn pointer"}`}
          disabled={disabled}
        >
          Start
        </button>
      </div>
      <div className="chooses">
        <h4 className="message">{whatDidIChoose}</h4>
        <h4 className="message">{whatDidCompChoose}</h4>
      </div>
      <h3 className="message">{decision}</h3>
      <h2 className="message">{final}</h2>
    </div>
  );
};

export default RPS;
