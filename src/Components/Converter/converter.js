import React, { useState, useEffect } from "react";
import "./converter.css";
import axios from "axios";
import { v4 as uuid } from "uuid";
import "../../App.css";

var App = () => {
  const [amount, setAmount] = useState(1);
  const [crcys, setCrcy] = useState([]);
  const [from, setFrom] = useState("EUR");
  const [to, setTo] = useState("USD");
  const [result, setResult] = useState("");

  useEffect(() => {
    async function fetchData() {
      let url =
        "http://www.apilayer.net/api/live?access_key=96a070b22045d72358f0d4961d1fc8ab";
      try {
        const res = await axios.get(url);
        var tempArr = [];
        for (var key in res.data.quotes) {
          tempArr.push({ crcyCode: key.slice(3), id: uuid() });
        }
        setCrcy(tempArr);
      } catch (error) {
        debugger;
      }
    }
    fetchData();
  }, []);

  useEffect(() => {
    actualConverter(amount, from, to);
  }, [amount, from, to, result]);

  const makeList = (key) => {
    return crcys.map((item) => {
      return item.crcyCode === "EUR" && key === "from" ? (
        <option value={item.crcyCode} key={item.id} selected>
          {item.crcyCode}
        </option>
      ) : item.crcyCode === "USD" && key === "to" ? (
        <option value={item.crcyCode} key={item.id} selected>
          {item.crcyCode}
        </option>
      ) : (
        <option value={item.crcyCode} key={item.id}>
          {item.crcyCode}
        </option>
      );
    });
  };
  const fromCrcy = (e) => {
    setFrom(e.target.value);
  };
  const toCrcy = (e) => {
    setTo(e.target.value);
  };

  const handleChange = (e) => {
    setAmount(e.target.value);
    actualConverter(amount, from, to);
  };

  const actualConverter = async (num, from, to) => {
    let url = `http://www.apilayer.net/api/live?access_key=96a070b22045d72358f0d4961d1fc8ab`;
    try {
      const res = await axios.get(url);
      let crcy1 = "USD" + from;
      let toUsd = num / res.data.quotes[crcy1];
      let crcy2 = "USD" + to;
      let data = (toUsd * res.data.quotes[crcy2]).toFixed(2);
      if (data !== "NaN") {
        setResult(data);
      } else {
        setResult(0);
      }
    } catch (error) {
      debugger;
    }
  };

  return (
    <div className="converter">
      <h3 className="title">🤑 Welcome to the Converter! 💰</h3>
      <div className="selectCont">
        <input
          className="input"
          type="text"
          placeholder="Amount..."
          value={amount}
          onChange={(e) => handleChange(e)}
        />
        <select onChange={fromCrcy} className="select pointer">
          {makeList("from")}
        </select>
      </div>
      <div className="selectCont">
        <p className="input default">{result}</p>
        <select onChange={toCrcy} className="select pointer">
          {makeList("to")}
        </select>
      </div>
    </div>
  );
};

export default App;
