import React, { useState } from "react";
import "./rot13.css";

const Rot13 = () => {
  const alphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];

  const [message, setMessage] = useState("");
  const [rot, setRot] = useState(13);
  let messageEncoded = "";

  for (let i = 0; i < message.length; i++) {
    for (let j = 0; j < alphabet.length; j++) {
      if (message[i] === alphabet[j]) {
        messageEncoded += alphabet[(j + rot) % 52].toLowerCase();
      } else if (message[i] === " ") {
        messageEncoded += " ";
      }
    }
  }

  const handleRot = (sign) => {
    if (sign === "+") {
      if (rot === 25) {
        setRot(0);
      } else {
        setRot(rot + 1);
      }
    } else if (sign === "-") {
      if (rot === 0) {
        setRot(25);
      } else {
        setRot(rot - 1);
      }
    }
  };

  return (
    <div className="rot13">
      <h3 className="title">🧙‍♀️ Welcome to ROT13 encoder 🔮</h3>
      <input
        className="textarea"
        onChange={(e) => setMessage(e.target.value)}
        value={message}
        placeholder="Secret message"
      />
      <button className="encodeBtn pointer">ROT {rot}</button>
      <div className="btnCont">
        <button className="btns" onClick={() => handleRot("+")}>
          +
        </button>
        <button className="btns" onClick={() => handleRot("-")}>
          -
        </button>
      </div>
      <p className="textarea default">{messageEncoded}</p>
    </div>
  );
};

export default Rot13;
