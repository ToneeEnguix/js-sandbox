import React, { useState } from "react";
import "./App.css";
import RPS from "./Components/RockPaperScissors/rockpaperscissors";
import Match from "./Components/MatchMe/matchme";
import Guess from "./Components/GuessTheNumber/guess";
import Rot13 from "./Components/rot13/rot13";
import Converter from "./Components/Converter/converter.js";

function App() {
  const [page, setPage] = useState("welcome");

  return (
    <div className="main">
      <div className="buttonsCont">
        <button
          onClick={() =>
            page !== "rockpaperscissors"
              ? setPage("rockpaperscissors")
              : setPage("welcome")
          }
          className="pointer buttons"
        >
          Rock, Paper, Scissors
        </button>
        <button
          onClick={() =>
            page !== "matchme" ? setPage("matchme") : setPage("welcome")
          }
          className="pointer buttons"
        >
          Match Me!
        </button>
        <button
          onClick={() =>
            page !== "guess" ? setPage("guess") : setPage("welcome")
          }
          className="pointer buttons"
        >
          Guess The Number
        </button>
        <button
          onClick={() =>
            page !== "rot13" ? setPage("rot13") : setPage("welcome")
          }
          className="pointer buttons"
        >
          ROT13 Encoder
        </button>
        <button
          onClick={() =>
            page !== "converter" ? setPage("converter") : setPage("welcome")
          }
          className="pointer buttons"
        >
          Converter
        </button>
      </div>
      {page === "welcome" ? (
        <div className="mainflex">
          <h2 className="title">Welcome!</h2>
          <h2 className="title">Choose a game 🎮</h2>
        </div>
      ) : page === "rockpaperscissors" ? (
        <RPS />
      ) : page === "matchme" ? (
        <Match />
      ) : page === "guess" ? (
        <Guess />
      ) : page === "converter" ? (
        <Converter />
      ) : (
        page === "rot13" && <Rot13 />
      )}
    </div>
  );
}

export default App;
