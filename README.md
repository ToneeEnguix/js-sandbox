// DESCRIPTION // 

This is a Sandbox to practice.

It has 5 tabs: 
- Rock, Paper, Scissors Game
    1. Press Start
    2. Choose your weapon
    3. Last person standing wins!
- Match Me (test by prompts Y/N )
    1. Press Start Match Test
    2. Write Person 1 name
    3. Write Person 2 name
    4. Type "Y" or "N" to answer prompts
    5. See if you're a perfect match!
- Guess The Number Game
    1. Press Start
    2. Type a number and press "Enter"
    3. Try until you guess the number!
- ROT13 Encoder
    1. Type the message you wish to encode
    2. Now you can copy your message and send your encoded text!
    3. To decode a text you received put in the input and see the message decoded
- Currency Converter (Live)
    1. Type any amount in the first input
    2. See the results in real time!


// LIVE DEMO //

www.itsjsgames.surge.sh
